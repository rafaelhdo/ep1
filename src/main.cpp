#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include "embarcacao.hpp"

using namespace std;

int main(){
    Embarcacao barco[12], *mapa[13][13];
    fstream arquivo;
    string str;

    arquivo.open("./doc/map_1.txt");

    if (arquivo.is_open()) {
        int i = 12;
        while (i) {
            getline(arquivo, str);
            i--;
        }
        int barcos_cont = 0;
        while (barcos_cont <= 5) {
            getline(arquivo, str);
            getline(arquivo, str);
            char *cstr = new char [str.length()+1];
            strcpy(cstr, str.c_str());
            char *pch = strtok(cstr, " ,.-");
            int posi[2] = {};
            for (int j = 0; j < 2; j++)
            {
                if (pch[1] != '\0') {
                    posi[j] = (pch[0] - '0') * 10;
                    posi[j] += (pch[1] - '0');
                }
                else {
                    posi[j] = (pch[0] - '0');
                }
                pch = strtok(NULL, " ,.-");
            }

            barco[barcos_cont].set_position(posi);

            barcos_cont++;
            delete cstr;
        }
    }
    else {
        cout << "não abriu\n";
    }

    arquivo.close();

    return 0;
}
