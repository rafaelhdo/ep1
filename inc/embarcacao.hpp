#ifndef EMBARCACAO_HPP
#define EMBARCACAO_HPP

#include <vector>

using namespace std;

class Embarcacao {
    private:
        int casas;
        int position[2];
    
    public:
        int get_casas();
        void set_casas(int casas);
        int get_position(int i);
        void set_position(int *position);
};


class Porta_Aviao : Embarcacao {
    private:

    public:
        void habilidade_especial();
};

class Submarino : Embarcacao {
    private:
    
    public:
        void habilidade_especial();
};

#endif